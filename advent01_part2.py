import math

def calculate(mass):
    fuel = math.floor(mass/ 3) - 2
    if fuel >= 0:
        return fuel
    elif fuel < 0:
        return 0

sum = 0

file = open ("input.txt", "r")
for line in file:
    mass = int(line)
    while mass > 0:
        fuel = calculate(mass)
        sum = sum + fuel
        mass = fuel

print(sum)
